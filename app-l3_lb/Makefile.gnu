CFLAGS+= -Wall -I. -I.. -fPIC
LDFLAGS+= -shared -Wl,-E
LDLIBS+= -lbsd -lm -lhashtab
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
all: $(OBJS)
	$(CC) $(LDFLAGS) -o l3_lb.so $(OBJS) $(LDLIBS)
install: l3_lb.so
	install -m 0644 l3_lb.so /usr/lib/
clean:
	rm -f *.o *.so
