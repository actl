/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef CONF_H
#define CONF_H

#include <libof.h>
#include <of10.h>

#include "actl.h"


#define CONFFILENAME	".actl"

typedef struct of_protocol	*(*ofp_fptr)(void);

#define	N_OFP_VERS		1
static const struct {
	char			*ver;
	ofp_fptr		func;
} ofp_vers[N_OFP_VERS] = {
	{"1.0", (ofp_fptr)of10_protocol}
}; 

#define LEARN_KMEANS_SEQ	0
#define LEARN_KMEANS_INCR	1
struct config {
	char			 *c_nodeid;		/* controller ID */
	int			  c_co_port;		/* control port */
	int			  c_sw_port;		/* switch port */
	char			 *c_ofp_verstr;		/* openflow version string */
	ofp_fptr		  c_ofp;		/* openflow version protocol */
	int		 	  c_nreplicas;		/* number of replicas */
	int			  c_npeers;		/* number of peers */
	struct peer {
		char		 *node_id;
		struct in_addr	  ip_addr;
		uint16_t	  port;
	} 			 *c_peers;		/* controller peers */
	char			 *c_app_name;		/* application name */
	int 			  c_app_argc;		/* app init() argc */
	char			**c_app_argv;		/* app init() argv */
	void			 *c_app_so;		/* app dlopen() handle */
	struct app		  c_app;		/* app handlers */
	int			  c_atime;		/* adaptation time */
	int			  c_ltime;		/* learning time */
	int			  c_lalgo;		/* learning algorithm */
	int			  c_lparam;		/* any algorithm-specific parameter */
	char			 *c_logfile;		/* log file name */
	FILE			 *c_logfp;		/* log file pointer */
	int			  c_topo_n;		/* size of topo arrary */
	struct host_info	**c_topo;		/* topology info */
};

/* parse.y */
int	parse_config(struct config *, const char *);
#endif
