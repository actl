/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef CLUSTERING_H
#define CLUSTERING_H

struct cluster {
	double		head;
	double		val;
	size_t		n_points;
};

struct kmeans_seq {
	size_t		 n_clusters;
	size_t		 n_points;
	struct cluster	*clusters;
};

#define	KMEANS_INCR_MAXTHRESH	1024
struct kmeans_incr {
	double		 threshold;
	size_t		 n_clusters;
	struct cluster	*clusters;
};

void kmeans_seq_init(struct kmeans_seq *, size_t);
void kmeans_seq_insert(struct kmeans_seq *, double, double);
double kmeans_seq_find(struct kmeans_seq *, double);
void kmeans_seq_print(struct kmeans_seq *, const char *);
void kmeans_incr_init(struct kmeans_incr *, double);
void kmeans_incr_insert(struct kmeans_incr *, double, double);
double kmeans_incr_find(struct kmeans_incr *, double);
void kmeans_incr_print(struct kmeans_incr *, const char *);

#endif
