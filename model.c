/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include "utils.h"
#include "model.h"

static uint64_t
nCr_r(uint32_t n, uint32_t r)
{
	if (n == r)
		return 1;
	if (r == 1)
		return n;
	return nCr_r(n - 1, r) + nCr_r(n - 1, r - 1);
}

static uint64_t
nCr(uint32_t n, uint32_t r)
{
	int		 i, j;
	uint64_t	*C[101];

	for (i = 0 ; i < 101 ; i++)
		C[i] = (uint64_t *)xcalloc(101, sizeof(uint64_t));
	for (i = 1 ; i <= n ; i++) {
		for (j = 1 ; j <= r ; j++) {
			if (i == j)
				C[i][j] = 1;
			else if (j == 1)
				C[i][j] = i;
			else
				C[i][j] = C[i - 1][j] + C[i - 1][j - 1];
		}
	}
	for (i = 0 ; i < 101 ; i++)
		free(C[i]);
	return C[n][r];
}

double
rw2phi(int r, int w, int n)
{
	if (r + w <= n)
		return 1.0 - ((double)nCr(n - w, r) / (double)nCr(n, r));
	else
		return 1.0;
}

void
phi2rw(double phi, int n, int *r, int *w)
{
	int		i, i_min = 1, j, j_min = 1;
	double		d, d_min = 1.1;

	for (i = 1 ; i < n ; i++) {
		for (j = i ; j <= n - i ; j++) {
			d = fabs(rw2phi(i, j, n) - phi);
			if (d < d_min) {
				d_min = d;
				i_min = i;
				j_min = j;
			}
		}
	}
	*r = i_min;
	*w = j_min;
}
