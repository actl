CFLAGS+= -Wall -I. -I.. -fPIC
LDFLAGS+= -shared -Wl,-E
LDLIBS+= -lbsd -lm -lhashtab
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
all: $(OBJS)
	$(CC) $(LDFLAGS) -o l2_hub.so $(OBJS) $(LDLIBS)
install: l2_hub.so
	install -m 0644 l2_hub.so /usr/lib/
clean:
	rm -f *.o *.so
